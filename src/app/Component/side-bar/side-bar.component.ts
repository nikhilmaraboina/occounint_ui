import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import {Router} from "@angular/router";
declare var $:any
@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {

  constructor(
    private router:Router
  ) { }

  ngOnInit(): void {
    $("#sidebarToggle, #sidebarToggleTop").on('click', function(e) {
      
      $("body").toggleClass("sidebar-toggled");

      $(".sidebar").toggleClass("toggled");
      if ($(".sidebar").hasClass("toggled")) {
          $('.sidebar .collapse').collapse('hide');
      };
  });

  
  }
  redirectTo(path: any) {
    this.router.navigateByUrl('/' + path);
  }
}
