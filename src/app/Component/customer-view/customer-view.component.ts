import { Component, OnInit } from '@angular/core';
import { customerservice } from '../../services/customerservice';
import Swal from'sweetalert2';
@Component({
  selector: 'app-customer-view',
  templateUrl: './customer-view.component.html',
  styleUrls: ['./customer-view.component.css']
})
export class CustomerViewComponent implements OnInit {

  customerinfo: any = [];
  totalRecords:any;
  page:number=1; 
  constructor(private apiService: customerservice) { 
    this.getAllDetails();
  }
 
  ngOnInit(): void {
  }

  arrayCustomerInfo:any=[];
  getAllDetails() {
    this.apiService.getCustomerDetails().subscribe((data) => {
      this.customerinfo = data;
     // this.totalRecords=this.customerInfo.length;
      this.totalRecords=this.customerinfo['allCustomerInfo'].length

    })
  }
  
  delete(customerId){

    Swal.fire({
      title: 'Are you Sure Delete the Customer',
      text: "",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ok'
    }).then((result) => {
      if (result.isConfirmed) {
        this.apiService.deleteCustomer(customerId).subscribe((data) => {
        Swal.fire(
       
          'Deleted Successfully'
        )
        this.getAllDetails();
        })
      }
    
    })
  }

  customerInfo:{};
adds;city;company;email;
fax;firstName;gstRegistrationType;lastName;
notes;phone;pincode;
state;
  getCustomerDetailesbyId(id){
    this.apiService.getCustomerById(id).subscribe(
      (data)=>{
        this.customerInfo=data['customerInfo'];
        this.adds=this.customerInfo['address']
        this.city=this.customerInfo['city']
        this.company=this.customerInfo['company']
        this.email=this.customerInfo['email']
        this.fax=this.customerInfo['fax']
        this.firstName=this.customerInfo['firstName']
        this.gstRegistrationType=this.customerInfo['gstRegistrationType']
        this.lastName=this.customerInfo['lastName']
        this.notes=this.customerInfo['notes']
        this.phone=this.customerInfo['phone']
        this.pincode=this.customerInfo['pincode']
        this.state=this.customerInfo['state']
        console.log(this.adds)
      },(error)=>{
        console.log(error);
      },()=>{

      }
    );
  }

}
