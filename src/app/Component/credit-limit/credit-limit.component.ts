import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { VendorService } from 'src/app/services/vendor.service';
import { ToastrService } from 'ngx-toastr'
import $ from 'jquery';
import { from } from 'rxjs';


@Component({
  selector: 'app-credit-limit',
  templateUrl: './credit-limit.component.html',
  styleUrls: ['./credit-limit.component.css']
})
export class CreditLimitComponent implements OnInit {

  vendorForm: FormGroup;
  Vendors: any = [];
  testData: any = {}
  submitted = false;
  creditupdateform: FormGroup;
  creditupdatedaysform: FormGroup;
  totalRecords: any;
  page: number = 1;

  constructor(
    private apiService: VendorService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService) {

  }
  creditlimitId;
  customerId;
  creditamount;
  salesId;
  creditDays;
  ngOnInit(): void {
    this.getallVendors();
    // console.log(this.creditlimitId)

    this.vendorForm = this.formBuilder.group({
      creditlimitId: [this.creditlimitId, Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")],
    });

    this.creditupdateform = this.formBuilder.group({
      customerId: [this.customerId, Validators.required,],
      creditamount: [this.creditamount, Validators.required],

    });


    this.creditupdatedaysform = this.formBuilder.group({
      salesId: [this.salesId, Validators.required],
      creditDays: [this.creditDays, Validators.required],

    });

  }

  id;
  credit;
  phone;

  customerArray: any = [];
  salesArray: any = [];
  finalArray: any = [];
  getallVendors() {
    this.apiService.getallVendors().subscribe(
      data => {
        this.Vendors = data;
        this.id = this.Vendors['id'];
        console.log(this.id)
        this.credit = this.Vendors['creditLimit'];
        this.phone = this.Vendors['phone'];
        console.log(this.Vendors);
        this.totalRecords = this.Vendors.length;

        for (let i = 0; i < this.Vendors.length; i++) {
          //  this.salesArray=this.Vendors[i].sales;
          //   console.log(this.Vendors[i].sales)
          this.salesArray.push(this.Vendors[i].sales);
        }
        console.log(this.salesArray)
      },
      error => {

        alert('error');
      });

  }
  creditLimit: String;
  get f() { return this.vendorForm.controls; }

  /* --------------------------------- Get Credit Amount -----------*/
  onSubmit() {
    this.submitted = true;

    if (!this.vendorForm.valid) {
      return false;
    } else {
      // console.log(this.vendorForm.value)
      this.creditlimitId = this.vendorForm.value.creditlimitId;

      this.apiService.getvendorbyId(this.creditlimitId).subscribe(
        data => {

          console.log(data)
          this.creditLimit = data['creditLimit'];
          console.log(this.creditLimit)
          // document.getElementyId("exampleModal").click();
          // $("#exampleModal").val("");

        },
        error => {
          if (error.status == 500) {
            console.log(error.error.message)
          } else if (error.status == 400) {
            console.log("sfsgf")
            console.log(error.message)
          } else {
            console.log("classed")
          }
          console.log(this.creditlimitId);
        });

    }

  }

  closeX() {
    $("#creditlimitId").val("");
    //$("#creditlimit").val("");
    $("#CreditA").val();
  }


  moreDetails(id) {

    this.testData = id;
    console.log(this.testData);


  }
  /* --------------------------------- Update Credit Amount -----------*/
  onSubmitCreditUpdate() {
    this.submitted = true;
    if (!this.creditupdateform.valid) {
      return false;
    } else {
      this.customerId = this.creditupdateform.value.customerId;
      this.creditamount = this.creditupdateform.value.creditamount;
      //  console.log(this.creditamount + " " + this.customerId);
      this.apiService.updatecreditLimit(this.customerId, this.creditamount).subscribe(
        (data) => {
          console.log(data)
          if (data['statusCode'] == 202) {
            this.toastr.success('Sucessfully   Credit Amount Updated', '', {
              timeOut: 2000
            });
          } else if (data['statusCode'] == 204) {
            this.toastr.error(data['message'], '', {
              timeOut: 2000
            });
            
          }
          else if (data['statusCode'] == 406) {
            this.toastr.error(data['message'], '', {
              timeOut: 2000
            });
          }
          // this.creditupdateform.reset();

        },
        (error) => {


          // if (error.status == 200) {
          //   console.log(error.error.message)
          // } else if (error.status == 204) {
          //   console.log("sfsgf")
          //   console.log(error.message)
          // } else {
          //   console.log("classed")
          // }
          // console.log(this.creditlimitId);



        }, () => {

        }
      );
    }

  }


  /* --------------------------------- Update Credit Days -----------*/
  onSubmitUpdatedays() {

    if (!this.creditupdatedaysform.valid) {
      return false;
    } else {
      this.salesId = this.creditupdatedaysform.value.salesId;
      this.creditDays = this.creditupdatedaysform.value.creditDays;
      console.log(this.salesId + " " + this.creditDays);
      this.apiService.updatecreditDays(this.salesId, this.creditDays).subscribe(
        (data) => {
          console.log(data)
          // this.creditupdatedaysform.reset();
          if (data['statusCode'] == 202) {
            this.toastr.success('Sucessfully   Credit Amount Updated', '', {
              timeOut: 2000
            });
          } else if (data['statusCode'] == 204) {
            this.toastr.error(data['message'], '', {
              timeOut: 2000
            });
          } else if (data['statusCode'] == 406) {
            this.toastr.error(data['message'], '', {
              timeOut: 2000
            });
          }


        }, (error) => {


          this.toastr.success('Sucessfully   Credit Days Updated', '', {
            timeOut: 2000
          });


        }, () => {

        }
      );
    }

  }
  creditDetails(id, phone, creditLimit) {
    console.log(id + " " + phone)
    this.id = id;
    this.phone = phone;
    this.credit = creditLimit;
  }

}
