import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormArray,
  FormControl,
  NgForm
} from '@angular/forms';
import { stringify } from 'querystring';
import { Router } from '@angular/router';
import Swal from'sweetalert2';

import { CustomergroupService } from '../../services/customergroup.service';
import { CustomerGroup } from '../CustomerGroup';
//import { trimValidator } from "../customergroup/trimvalidate";
@Component({
  selector: 'app-customergroup',
  templateUrl: './customergroup.component.html',
  styleUrls: ['./customergroup.component.css']
})
export class CustomergroupComponent implements OnInit {
  form: FormGroup;
  customerGroupForm: FormGroup;
  submitted = false;  
  totalRecords:any;
  page:number=1; 
  get selectedCustomersArray() {
    return this.form.controls.customers as FormArray;
  }
  constructor(private formBuilder: FormBuilder, private service: CustomergroupService,private router: Router) {
    this.form = this.formBuilder.group({
      customers: new FormArray([])

    });
  }

  

  checkboxesDataList: any = []

  getAllCustomersData() {
    this.service.getallCustomersData().subscribe(
      (data) => {
        this.checkboxesDataList = data;
        this.addCheckboxes();
        // this.router.navigate(['/customergroupingview'])
        console.log(this.checkboxesDataList)
      }, (error) => {
        if (error.status == 400) {
          //this.service.errorAlert(error.error.message);

        } else if (error.status == 500) {
          // this.service.errorAlert(error.error.message);
        }
      }
    );
  }

  private addCheckboxes() {
    this.checkboxesDataList.forEach(() => this.selectedCustomersArray.push(new FormControl(false)));
  }

  ngOnInit() {
    this.getAllCustomersData();

  }


  selectedCustomers: any = [];
  submit() {

    this.selectedCustomers = this.form.value.customers
      .map((checked, i) => checked ? this.checkboxesDataList[i] : null)
      .filter(v => v !== null);
    console.log(this.selectedCustomers);
  }

  massage: {};
  validationMessage: String;
  unamePattern = "^[a-zA-Z0-9_-]{4,20}$";
 // pwdPattern = "^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{6,12}$";
//  mobnumPattern = "^((\\+91-?)|0)?[0-9]{10}$";
//  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  isValidFormSubmitted = false;
  validateEmail = true;
  customerGroupModel = new CustomerGroup();
  onFormSubmit(form: NgForm) {
    this.isValidFormSubmitted = false;
    if (form.invalid) {
      console.log("false")
      return;
    }
  //  console.log()
    this.isValidFormSubmitted = true;
    this.customerGroupModel.groupId = 0;
    this.customerGroupModel.groupName = form.value['groupName'];
    this.customerGroupModel.customers = this.selectedCustomers;

    console.log(this.customerGroupModel)

    this.service.createCustomerGroup(this.customerGroupModel).subscribe(
      (data) => {
        console.log(data)
        this.massage = data;
        if (data['true']) {
          this.validationMessage = "Group Created Successfully";
          document.getElementById('customergroup').click();
          this.router.navigate(['customergroupingview']);
          Swal.fire("Customer Group Created Successfully");
        } else {
          this.validationMessage = "Group name already exists.Try with another one";
        }

        //   this.service.successAlert(this.massage);
      }, (error) => {
        if (error.status == 400) {

          this.validationMessage = error.error.message;
        } else if (error.status == 500) {
          this.validationMessage = error.error.message;
        }
      }, () => {
        form.resetForm();
      }
    );
  }

}