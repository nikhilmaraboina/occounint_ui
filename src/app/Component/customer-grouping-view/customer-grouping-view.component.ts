import { Component, OnInit } from '@angular/core';
import { CustomergroupService } from '../../services/customergroup.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-customer-grouping-view',
  templateUrl: './customer-grouping-view.component.html',
  styleUrls: ['./customer-grouping-view.component.css']
})
export class CustomerGroupingViewComponent implements OnInit {

  public groupList: any[];
  totalRecords: any;
  page: number = 1;
  constructor(private service: CustomergroupService) { }

  ngOnInit(): void {
    this.getAllCustomersGroupData();
  }
  customersAllGroupName: any = [];
  getAllCustomersGroupData() {
    this.service.getallCustomersGroupData().subscribe(
      (data) => {
        console.log(data)
        this.customersAllGroupName = data;
        this.totalRecords = this.customersAllGroupName.length;
      }, (error) => {
        console.log(error)
      }
    );
  }
  customerGroupId;
  
  deleteGroupCustomer(id) {  
    Swal.fire({
      title: 'Are you Sure Delete the Customer Group',
      text: "",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ok'
    }).then((result) => {
      if (result.isConfirmed) {
        this.service.deleteCustomerGroup(id).subscribe((data) => {
        Swal.fire(
          'Deleted Successfully'
        )
        this.getAllCustomersGroupData();
        })
      }
    
    })
  }


}
