import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChartAccountsComponent } from './Component/chart-accounts/chart-accounts.component';
import { CostcenterAddComponent } from './Component/costcenter-add/costcenter-add.component';
import { CostcenterUpdateComponent } from './Component/costcenter-update/costcenter-update.component';
import { CostcenterViewComponent } from './Component/costcenter-view/costcenter-view.component';
import { CreditLimitComponent } from './Component/credit-limit/credit-limit.component';
import { CustomerGroupingViewComponent } from './Component/customer-grouping-view/customer-grouping-view.component';
import { CustomergroupComponent } from './Component/customergroup/customergroup.component';
import { DashboardComponent } from './Component/dashboard/dashboard.component';
import { ExpensesComponent } from './Component/expenses/expenses.component';
import { GetallVendorsComponent } from './Component/getall-vendors/getall-vendors.component';
import { LoginComponent } from './Component/login/login.component';
import { ProductmasterAddComponent } from './Component/productmaster-add/productmaster-add.component';
import { ProductmasterUpdateComponent } from './Component/productmaster-update/productmaster-update.component';
import { ProductmasterViewComponent } from './Component/productmaster-view/productmaster-view.component';
import { RegisterComponent } from './Component/register/register.component';
import { UpdateallVendorsComponent } from './Component/updateall-vendors/updateall-vendors.component';
import { UpdategroupingcustomerComponent } from './Component/updategroupingcustomer/updategroupingcustomer.component';
import { VendorAddComponent } from './Component/vendor-add/vendor-add.component';
import { VendorUpdateComponent } from './Component/vendor-update/vendor-update.component';
import { VendorViewComponent } from './Component/vendor-view/vendor-view.component';
import { WarehouseDeleteComponent } from './Component/warehouse-delete/warehouse-delete.component';
import { WarehouseGroupingListViewComponent } from './Component/warehouse-grouping-list-view/warehouse-grouping-list-view.component';
import { WarehouseGroupingComponent } from './Component/warehouse-grouping/warehouse-grouping.component';
import { WarehouseListComponent } from './Component/warehouse-list/warehouse-list.component';
import { WarehouseUpdateComponent } from './Component/warehouse-update/warehouse-update.component';
import { CostcenterGroupingComponent } from "src/app/Component/costcenter-grouping/costcenter-grouping.component"
import { CostcenterGroupingViewComponent } from "src/app/Component/costcenter-grouping-view/costcenter-grouping-view.component"
import { CustomerAddComponent } from './Component/customer-add/customer-add.component';
import { CustomerViewComponent } from './Component/customer-view/customer-view.component';
import { CustomerUpdateComponent } from './Component/customer-update/customer-update.component';
import { AuthenticationGuard } from './auth/authentication.guard';



const routes: Routes = [
  {path:"",component:DashboardComponent, pathMatch:'full',canActivate:[AuthenticationGuard]},
  {path:'dashboard',component:DashboardComponent,canActivate:[AuthenticationGuard]},
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent,canActivate:[AuthenticationGuard]},
  { path: 'warehouselist', component: WarehouseListComponent,canActivate:[AuthenticationGuard] },
  { path: 'warehouseUpdate', component: WarehouseUpdateComponent,canActivate:[AuthenticationGuard]  },
  { path: 'warehouseDelete', component: WarehouseDeleteComponent ,canActivate:[AuthenticationGuard] },
  { path: 'allvendors', component: GetallVendorsComponent ,canActivate:[AuthenticationGuard] },
  { path: 'updatevendor', component: UpdateallVendorsComponent ,canActivate:[AuthenticationGuard] },
  { path: 'customergroup', component: CustomergroupComponent,canActivate:[AuthenticationGuard]  },
  { path: 'customergroupingview', component: CustomerGroupingViewComponent ,canActivate:[AuthenticationGuard] },
  { path: 'productmaster-add', component: ProductmasterAddComponent ,canActivate:[AuthenticationGuard] },
  { path: 'productmaster-view', component: ProductmasterViewComponent ,canActivate:[AuthenticationGuard] },
  { path: 'vendorAdd', component: VendorAddComponent,canActivate:[AuthenticationGuard]  },
  { path: 'vendorView', component: VendorViewComponent,canActivate:[AuthenticationGuard]  },
  { path: 'vendorUpdate', component: VendorUpdateComponent ,canActivate:[AuthenticationGuard] },
  { path: 'updategroupcustomer/:id', component: UpdategroupingcustomerComponent,canActivate:[AuthenticationGuard]  },
  { path: 'creditlimit', component: CreditLimitComponent ,canActivate:[AuthenticationGuard] },
  { path: 'productMasterUpdate', component: ProductmasterUpdateComponent,canActivate:[AuthenticationGuard]  },
  { path: 'warehouseGrouping', component: WarehouseGroupingComponent ,canActivate:[AuthenticationGuard] },
  { path: 'WHouseGroupListView', component: WarehouseGroupingListViewComponent ,canActivate:[AuthenticationGuard] },
  { path: 'chartAccount', component: ChartAccountsComponent ,canActivate:[AuthenticationGuard] },
  { path: 'expense', component: ExpensesComponent ,canActivate:[AuthenticationGuard] },
  { path: 'costCenterAdd', component: CostcenterAddComponent ,canActivate:[AuthenticationGuard] },
  { path: 'costCenterView', component: CostcenterViewComponent,canActivate:[AuthenticationGuard]  },
  // { path: "costCenterUpdate/:id", component: CostcenterUpdateComponent },
  { path: 'costCenterUpdate', component: CostcenterUpdateComponent,canActivate:[AuthenticationGuard]  },
  { path: 'costCenterGrouping', component: CostcenterGroupingComponent,canActivate:[AuthenticationGuard]  },
  { path: 'costCenterGroupingView', component: CostcenterGroupingViewComponent,canActivate:[AuthenticationGuard]  },
  { path: 'costCenterGrouping', component: CostcenterGroupingComponent ,canActivate:[AuthenticationGuard] },
  { path: 'costCenterGroupingView', component: CostcenterGroupingViewComponent ,canActivate:[AuthenticationGuard] },
  { path: "customer-add", component: CustomerAddComponent ,canActivate:[AuthenticationGuard] },
  { path: "customer-view", component: CustomerViewComponent,canActivate:[AuthenticationGuard]  },
  { path: "customer-update/:id", component: CustomerUpdateComponent ,canActivate:[AuthenticationGuard] }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
